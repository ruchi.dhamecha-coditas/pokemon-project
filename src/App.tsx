import { useState } from 'react'
import Pokemon from './component/Pokemon/Pokemon'
import style from './App.module.scss'
import PokemonList from './component/Pokemon list/PokemonList'

function App() {
  return(
    <>
    <div className={style.Main}>
      <Pokemon/>
      <PokemonList/>
    </div>
     
    </>
  )
  }

export default App
