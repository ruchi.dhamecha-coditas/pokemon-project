import axios from "axios";
import { useEffect, useState } from "react";
// import style from './pokemon.module.scss';
import style from "./pokemon.module.scss";

const Pokemon = () => {
  const data = [
    { id: 1, name: "Bulbasaur" },
    { id: 2, name: "Ivysaur" },
    { id: 3, name: "Venusaur" },
    { id: 4, name: "Charmander" },
    { id: 5, name: "Charmeleon" },
    { id: 6, name: "Charizard" },
    { id: 7, name: "Squirtle" },
    { id: 8, name: "Wartortle" },
    { id: 9, name: "Blastoise" },
  ];

  function addToList(){
    // data.filter()
  }

  // const [myname , setmyName] = useState([]);
  // useEffect(()=>{
  //     axios
  //             .get("https://18be-47-247-159-210.ngrok-free.app/pokemons")
  //             .then((res)=>{
  //                 // console.log("hi");
  //                 console.log(res);
  //                 // setmyName(res.data);
  //             })
  // },[])

  return (
    <>

    <div>
    <h1>Pokemon types</h1>
      {data.map((ptypes) => {
        const { id, name } = ptypes;

        return (
          <div className={style.List}>
            <p>{id}</p>
            <p>{name}</p>
            <button onClick={()=> addToList}>Add to list</button>
          </div>
        );
      })}
    </div>
     
    </>
  );
};

export default Pokemon;
